import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { IProp } from 'app/shared/model/prop';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared';
import { ICalc } from 'app/shared/model/calc';

type EntityResponseType = HttpResponse<IProp>;
type EntityArrayResponseType = HttpResponse<IProp[]>;

@Injectable({ providedIn: 'root' })
export class ExpressionLanguageService {
    private resourceUrl = SERVER_API_URL + 'api/expression-language';

    constructor(private http: HttpClient) {}

    createProperties(data: IProp): Observable<EntityResponseType> {
        return this.http.post<IProp>(this.resourceUrl, data, { observe: 'response' });
    }

    updateProperties(data: IProp): Observable<EntityResponseType> {
        return this.http.put<IProp>(this.resourceUrl, data, { observe: 'response' });
    }

    deleteProperties(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    queryProperties(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IProp[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    calculate(data: ICalc): Observable<HttpResponse<ICalc>> {
        return this.http.post<ICalc>(this.resourceUrl + '/calc', data, { observe: 'response' });
    }
}
