import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExpressionLanguageComponent } from './expression-language.component';
import { BaseSharedModule } from 'app/shared';
import { expressionLanguageRoute } from './expression-language.route';
import { CalculateComponent } from '../calculate/calculate.component';
import { PropComponent } from '../properties/properties.component';

@NgModule({
    imports: [BaseSharedModule, RouterModule.forChild([expressionLanguageRoute])],
    declarations: [ExpressionLanguageComponent, CalculateComponent, PropComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [ExpressionLanguageComponent, CalculateComponent, PropComponent]
})
export class BaseExpLangModule {}
