import { Route } from '@angular/router';
import { ExpressionLanguageComponent } from './expression-language.component';
import { CalculateComponent } from '../calculate/calculate.component';
import { PropComponent } from '../properties/properties.component';

export const expressionLanguageRoute: Route = {
    path: 'expression-language',
    component: ExpressionLanguageComponent,
    children: [{ path: 'calculate', component: CalculateComponent }, { path: 'prop', component: PropComponent }]
};
