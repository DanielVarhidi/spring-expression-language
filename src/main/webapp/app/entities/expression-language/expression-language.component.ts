import { Component, OnInit } from '@angular/core';
import { IProp } from 'app/shared/model/prop';
import { ExpressionLanguageService } from './expression-language.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ICalc } from 'app/shared/model/calc';
import { find } from 'rxjs/operators';

@Component({
    selector: 'jhi-expression-language',
    templateUrl: './expression-language.component.html',
    styles: []
})
export class ExpressionLanguageComponent implements OnInit {
    result: ICalc;
    fieldArray: Array<IProp> = [];

    constructor(private expLangService: ExpressionLanguageService) {}

    ngOnInit() {
        this.loadAllProp();
    }

    loadAllProp() {
        this.expLangService.queryProperties().subscribe((res: HttpResponse<IProp[]>) => {
            this.fieldArray = res.body;
        });
    }

    createProp(newAttribute: any) {
        this.expLangService.createProperties(newAttribute).subscribe((res: HttpResponse<IProp>) => {
            this.fieldArray.push(res.body);
        });
    }

    deleteProp(id) {
        this.expLangService.deleteProperties(id).subscribe((res: HttpResponse<IProp>) => {
            this.loadAllProp();
        });
    }

    updateProp(id) {
        let item: IProp;
        this.fieldArray.forEach(element => {
            if (element.id === id) {
                item = element;
            }
        });

        this.expLangService.updateProperties(item).subscribe((res: HttpResponse<IProp>) => {
            for (let i = 0; i < this.fieldArray.length; ++i) {
                if (this.fieldArray[i].id === res.body.id) {
                    this.fieldArray[i] = res.body;
                }
            }
        });
    }

    calculate(data) {
        this.expLangService.calculate(data).subscribe((res: HttpErrorResponse<ICalc>) => {
            this.result = res.body;
            console.log(this.result);
        });
    }
}
