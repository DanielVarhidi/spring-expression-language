import { Component, OnInit } from '@angular/core';
import { IProp } from '../../shared/model/prop';
import { ExpressionLanguageComponent } from '../expression-language/expression-language.component';

@Component({
    selector: 'jhi-prop',
    templateUrl: './properties.component.html',
    styles: []
})
export class PropComponent implements OnInit {
    private newAttribute: any = {};

    addFieldValue() {
        this.expLangComp.createProp(this.newAttribute);
        this.newAttribute = {};
    }

    deleteFieldValue(id) {
        this.expLangComp.deleteProp(id);
    }

    updateFieldValue(id) {
        this.expLangComp.updateProp(id);
    }

    constructor(private expLangComp: ExpressionLanguageComponent) {}

    ngOnInit() {}
}
