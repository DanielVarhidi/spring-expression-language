import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BaseExpLangModule } from './expression-language/expression-language.module';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [BaseExpLangModule],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BaseEntityModule {}
