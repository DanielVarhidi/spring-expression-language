import { Component, OnInit } from '@angular/core';
import { ExpressionLanguageComponent } from '../expression-language/expression-language.component';

@Component({
    selector: 'jhi-calculate',
    templateUrl: './calculate.component.html',
    styles: []
})
export class CalculateComponent implements OnInit {
    isShowResult = false;
    text: string;

    constructor(private expLangComp: ExpressionLanguageComponent) {}

    ngOnInit() {}

    onSubmit(text) {
        let data = { text: text };
        this.expLangComp.calculate(data);
        this.isShowResult = true;
    }
}
