import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, NavigationEnd } from '@angular/router';

import { JhiLanguageHelper, LANGUAGES } from 'app/core';
import { JhiLanguageService } from 'ng-jhipster';
import * as $ from 'jquery';

@Component({
    selector: 'jhi-main',
    templateUrl: './main.component.html'
})
export class JhiMainComponent implements OnInit {
    constructor(private jhiLanguageService: JhiLanguageService, private jhiLanguageHelper: JhiLanguageHelper, private router: Router) {}

    private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title: string = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : 'baseApp';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }

    ngOnInit() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.jhiLanguageHelper.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
            }
        });

        // use browser language as default. Use 'en' if browser language not allowed by the application
        //        let userLang = navigator.language || navigator.userLanguage;
        let userLang = navigator.language.substring(0, 2);
        if (!LANGUAGES.includes(userLang)) {
            userLang = LANGUAGES[0];
        }
        this.jhiLanguageService.changeLanguage(userLang);
        $('body').on('click', 'input[type="datetime-local"]', function() {
            if (!($(this).attr('max') !== undefined && $(this).attr('max') !== false)) {
                $(this).attr('max', '9999-12-31T21:25:33');
            }
        });
        $('body').on('click', 'input[type="date"]', function() {
            if (!($(this).attr('max') !== undefined && $(this).attr('max') !== false)) {
                $(this).attr('max', '9999-12-31');
            }
        });
    }
}
