export interface IProp {
    id: string;
    key: boolean;
    value: string;
}

export class Prop implements IProp {
    constructor(public id: string, public key: boolean, public value: string) {}
}
