export interface ICalc {
    text: string;
    type: string;
    resultvalue: string;
}

export class Clac implements ICalc {
    constructor(public text: string, public type: string, public resultvalue: string) {}
}
