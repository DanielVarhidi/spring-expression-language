import { Directive, Input, SimpleChanges } from '@angular/core';
import { Validator, AbstractControl, Validators } from '@angular/forms';
import { NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[minDate]',
    providers: [{ provide: NG_VALIDATORS, useExisting: MinDateDirective, multi: true }]
})
export class MinDateDirective implements Validator {
    @Input('minDate') minDate: string;

    private _onChange: () => void;

    constructor() {}

    validate(c: AbstractControl) {
        const debug = false;
        const value = c.value;
        if (debug) {
            console.log('minDateDirective value: ' + value);
            console.log('minDateDirective this.minDate: ' + this.minDate);
        }
        if (this.minDate === 'today') {
            const d = new Date();
            this.minDate =
                d.getFullYear() +
                '-' +
                (d.getMonth() + 1 < 10 ? '0' : '') +
                (d.getMonth() + 1) +
                '-' +
                (d.getDate() < 10 ? '0' : '') +
                d.getDate() +
                'T' +
                (d.getHours() < 10 ? '0' : '') +
                d.getHours() +
                ':' +
                (d.getMinutes() < 10 ? '0' : '') +
                d.getMinutes();
            if (debug) {
                console.log('minDateDirective this.minDate current: ' + this.minDate);
            }
        }

        const dvalue = Date.parse(value);
        const dmd = Date.parse(this.minDate);
        if (debug) {
            console.log('minDateDirective dvalue ' + dvalue);
            console.log('minDateDirective dmd ' + dmd);
            console.log('minDateDirective dvalue<dmd ' + (dvalue < dmd));
        }

        if ((value === null || value === undefined || value === '') && this.minDate) {
            if (debug) {
                console.log('minDateDirective value: ' + value);
                console.log('minDateDirective this.minDate: ' + this.minDate);
                console.log('minDateDirective return: ');
            }
            const ret = {
                minDate: { condition: this.minDate }
                // minDate: {condition:dvalue>dmd}
            };
            return ret;
        } else if (dmd > dvalue) {
            if (debug) {
                console.log('minDateDirective return null less than min');
            }
            const retm = {
                minDate: { condition: this.minDate }
                // minDate: {condition:dvalue>dmd}
            };
            return retm;
        } else {
            if (debug) {
                console.log('minDateDirective return null');
            }
            return null;
        }
    }

    registerOnValidatorChange(fn: () => void): void {
        this._onChange = fn;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if ('minDate' in changes) {
            if (this._onChange) {
                this._onChange();
            }
        }
    }
}
