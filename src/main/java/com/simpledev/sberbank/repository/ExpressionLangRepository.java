package com.simpledev.sberbank.repository;

import com.simpledev.sberbank.domain.ExpressionLang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpressionLangRepository extends JpaRepository<ExpressionLang, Long> {
}
