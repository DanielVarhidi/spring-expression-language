package com.simpledev.sberbank.domain;

import java.util.HashMap;
import java.util.Map;

public class CalculationData {

    String text;

    String resultValue;

    public String getResultValue() {
        return resultValue;
    }

    public void setResultValue(String resultValue) {
        this.resultValue = resultValue;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return "CalculationData{" +
            "text='" + text + '\'' +
            '}';
    }
}
