package com.simpledev.sberbank.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "expression_language")
public class ExpressionLang implements Serializable {
    private static final String INTEGER = "INTEGER";
    private static final String DOUBLE = "DOUBLE";
    private static final String STRING = "STRING";
    private static final String DATE = "DATE";

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String key;

    @NotNull
    @Column(nullable = false)
    private String value;

    @NotNull
    @Column(nullable = false)
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExpressionLang that = (ExpressionLang) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(key, that.key) &&
            Objects.equals(value, that.value);
    }

    public Object getConvertedValue() {
        Object retVal = null;
        try {
            switch (type) {
                case INTEGER:
                    retVal = Integer.parseInt(value);
                    break;
                case DOUBLE:
                    retVal = Double.parseDouble(value);
                    break;
                case STRING:
                    retVal = String.valueOf(value);
                    break;
                case DATE:
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    retVal = format.parse(value);
                    break;
                default:
                    break;
            }
        } catch(NumberFormatException ex) {

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return retVal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, key, value);
    }

    @Override
    public String toString() {
        return "ExpressionLang{" +
            "id=" + id +
            ", key='" + key + '\'' +
            ", value='" + value + '\'' +
            '}';
    }

}
