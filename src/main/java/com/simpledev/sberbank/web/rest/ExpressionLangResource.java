package com.simpledev.sberbank.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.simpledev.sberbank.domain.CalculationData;
import com.simpledev.sberbank.domain.ExpressionLang;
import com.simpledev.sberbank.service.ExpressionLangService;
import com.simpledev.sberbank.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ExpressionLangResource {
    private static final Logger log = LoggerFactory.getLogger(ExpressionLangResource.class);
    private static final String ENTITY_NAME = "expressionLanguage";

    @Autowired
    private ExpressionLangService expressionLangService;
/*
    @Autowired
    public ExpressionLangResource(ExpressionLangService expressionLangService){
        this.expressionLangService = expressionLangService;
    }
*/
    @RequestMapping(
        value = "/expression-language",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Timed
    public ResponseEntity<ExpressionLang> createExpressionLang(@Valid @RequestBody ExpressionLang expressionLang) throws URISyntaxException {
        log.debug("REST request to save ExpressionLang : {} ", expressionLang);

        if(expressionLang.getId() != null){
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists","A new expressionLang cannot already have an ID")).body(null);
        }
        ExpressionLang result = expressionLangService.saveExpressionLang(expressionLang);

        return ResponseEntity.created(new URI("/api/expression-language"+result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @RequestMapping(
        value = "/expression-language",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Timed
    public ResponseEntity<ExpressionLang> updateExpressionLang(@Valid @RequestBody ExpressionLang expressionLang){
        log.debug("REST request to update ExpressionLang : {}", expressionLang);

        if (expressionLang.getId() == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idnull", "Invalid id")).body(null);
        }

        ExpressionLang result = expressionLangService.saveExpressionLang(expressionLang);


        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, expressionLang.getId().toString()))
            .body(result);
    }

    @RequestMapping(
        value = "/expression-language",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Timed
    public ResponseEntity<List<ExpressionLang>> getAllExpressionLang() throws URISyntaxException {
        log.debug("REST request to get a page of ExpressionLang");

        List<ExpressionLang> result = expressionLangService.findAllExpressionLang();

        return ResponseEntity.created(new URI("/api/expression-language"))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString()))
            .body(result);
    }


    @RequestMapping(value = "/expression-language/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteExpressionLang(@PathVariable Long id){
        log.debug("REST request to delete ExpressionLang : {}", id);

        ExpressionLang expressionLang = expressionLangService.findOne(id);
        expressionLangService.deleteExpressionLang(id);

        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @RequestMapping(
        value = "/expression-language/calc",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Timed
    public ResponseEntity<CalculationData> startCalculation(@Valid @RequestBody CalculationData calculationData) throws URISyntaxException {
        log.debug("REST request to start startCalc() method : {}", calculationData);
        CalculationData result = startCalc(calculationData);
        String str = String.valueOf(result);
        return ResponseEntity.created(new URI("/api/expression-language"))
            .headers(HeaderUtil.createEntityCreationAlert("CalcResult", result.toString()))
            .body(result);
    }

    public CalculationData startCalc(CalculationData calculationData){
        List<ExpressionLang> variableList = expressionLangService.findAllExpressionLang();
        ExpressionParser parser = new SpelExpressionParser();

        Expression exp = parser.parseExpression(calculationData.getText());
        EvaluationContext context = new StandardEvaluationContext(calculationData);
        for(ExpressionLang variable : variableList) {
            context.setVariable(variable.getKey(), variable.getConvertedValue());
        }

        String strResult = exp.getValue(context, String.class);
        calculationData.setResultValue(strResult);

        return calculationData;

    }

    /*
    public void testCalc(CalculationData calculationData){
        //CalculationData objektumban megkeresi a text változót és kiveszi az értékét
        ExpressionParser parser = new SpelExpressionParser();
        Expression exp = parser.parseExpression("text");
        EvaluationContext context = new StandardEvaluationContext(calculationData);
        String calculationDataText = (String) exp.getValue(context);
        System.out.println("Value of CalculationData.text: "+calculationDataText);

        //Operátorok használata: CalculationData.text vizsgálata
        Expression exp1 = parser.parseExpression("text < '1111'");
        EvaluationContext context1 = new StandardEvaluationContext(calculationData);
        boolean result = exp1.getValue(context1, Boolean.class);
        System.out.println("Is CalculationData.text is < 1111: "+result);

        //Típusba kényszerítés
        // evals to "Hello World"
        String helloWorld = (String) parser.parseExpression("'Hello World'").getValue();
            System.out.println("helloWorld: "+helloWorld);
        double avogadrosNumber = (Double) parser.parseExpression("6.0221415E+23").getValue();
            System.out.println("avogadrosNumber: "+avogadrosNumber);
        // evals to 2147483647
        int maxValue = (Integer) parser.parseExpression("0x7FFFFFFF").getValue();
            System.out.println("maxValue: "+maxValue);
        boolean trueValue = (Boolean) parser.parseExpression("true").getValue();
            System.out.println("trueValue: "+trueValue);
        Object nullValue = parser.parseExpression("null").getValue();
            System.out.println("nullValue: "+nullValue);

            //
        // evaluates to false
        boolean falseValue = parser.parseExpression(
            "'xyz' instanceof T(Integer)").getValue(Boolean.class);
        System.out.println("falseValue: "+falseValue);

        // evaluates to true
        boolean trueValue1 = parser.parseExpression(
            "'5.00' matches '\^-?\\d+(\\.\\d{2})?$'").getValue(Boolean.class);
        System.out.println("trueValue1: "+trueValue1);
        //evaluates to false
        boolean falseValue1 = parser.parseExpression(
            "'5.0067' matches '\^-?\\d+(\\.\\d{2})?$'").getValue(Boolean.class);
        System.out.println("falseValue1: "+falseValue1);


        //Change CalculationData.text
        System.out.println("CalculationData.text: "+calculationDataText);
        StandardEvaluationContext context3 = new StandardEvaluationContext(calculationData);
        context3.setVariable("newText","New String");
        parser.parseExpression("text = #newText").getValue(context3);
        Expression exp3 = parser.parseExpression("text");
        EvaluationContext context4 = new StandardEvaluationContext(calculationData);
        String calculationDataText4 = (String) exp3.getValue(context4);
        System.out.println("Changed CalculationData.text: "+calculationDataText4);

    }
    */
}
