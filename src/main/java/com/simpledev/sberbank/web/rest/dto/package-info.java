/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.simpledev.sberbank.web.rest.dto;
