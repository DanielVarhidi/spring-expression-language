package com.simpledev.sberbank.service;

import com.simpledev.sberbank.domain.ExpressionLang;
import com.simpledev.sberbank.repository.ExpressionLangRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Service
@Transactional
public class ExpressionLangService {

    @Inject
    private ExpressionLangRepository expressionLangRepository;

    public ExpressionLang saveExpressionLang(ExpressionLang expressionLang){
        return expressionLangRepository.save(expressionLang);
    }

    public List<ExpressionLang> findAllExpressionLang() {
        return expressionLangRepository.findAll();
    }

    public ExpressionLang findOne(Long id) {return expressionLangRepository.findOne(id);}

    public void deleteExpressionLang(Long id){
        expressionLangRepository.delete(id);
    }

}
